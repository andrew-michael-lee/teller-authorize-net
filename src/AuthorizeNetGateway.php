<?php

namespace Teller\AuthorizeNet;

use net\authorize\api\contract\v1 as ANetAPI;
use net\authorize\api\constants as ANetConstants;
use net\authorize\api\controller as ANetController;

use Exception;
use Teller\AuthorizeNet\Action\Action;
use Teller\AuthorizeNet\Action\AuthorizeAction;
use Teller\AuthorizeNet\Action\CapturePriorAuthorizationAction;
use Teller\AuthorizeNet\Action\ChargeAction;
use Teller\AuthorizeNet\Support\GatewayResponse;
use Teller\Contracts\Gateway;
use Teller\Entity\Authorization;
use Teller\Entity\Capture;
use Teller\Entity\CreditCard;
use Teller\Entity\Order;
use Teller\Entity\Payment;
use Teller\Entity\Person;

/**
 * Class AuthorizeNetDriver
 *
 * @package Teller\AuthorizeNet
 */
class AuthorizeNetGateway implements Gateway
{
    const RESPONSE_OK = "Ok";
    const RESPONSE_ERROR = "Error";

    private $merchantId;
    private $merchantTransactionKey;
    private $environment;

    public function __construct( String $merchantId, String $merchantTransactionKey, String $environment )
    {
        $this->merchantId = $merchantId;
        $this->merchantTransactionKey = $merchantTransactionKey;
        $this->environment = $environment;
    }

    /**
     * Convenience method for executing gateway transactions.
     *
     * @param Action $action
     * @return GatewayResponse
     * @throws Exception
     */
    private function execute( Action $action ) : GatewayResponse
    {
        // Assemble the complete transaction request
        $request = new ANetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication( $this->getMerchantAuthentication() );
        $request->setRefId( $this->generateReferenceId() );
        $request->setTransactionRequest( $action->getTransactionRequestType() );

        // Create the controller and get the response
        $controller = new ANetController\CreateTransactionController( $request );
        $response = $controller->executeWithApiResponse( $this->getEndpoint() );

        // throw an error if there was no response from the gateway
        if( null == $response )
        {
            throw new Exception( "No response returned from gateway." );
        }

        // throw an exception if the response or the transaction response contain an error or error message
        if( $response->getMessages()->getResultCode() != AuthorizeNetGateway::RESPONSE_OK )
        {
            $tresponse = $response->getTransactionResponse();
            if( $tresponse != null && $tresponse->getErrors() != null )
            {
                error_log( "Exception: [" . $tresponse->getErrors()[0]->getErrorCode() . "] - " . $tresponse->getErrors()[0]->getErrorText() );
                throw new Exception( $tresponse->getErrors()[0]->getErrorText(), $tresponse->getErrors()[0]->getErrorCode() );
            }
            else
            {
                error_log( "Exception: [" . $response->getMessages()->getMessage()[0]->getCode() . "] - " . $response->getMessages()->getMessage()[0]->getText() );
                throw new Exception( $response->getMessages()->getMessage()[0]->getText(), $response->getMessages()->getMessage()[0]->getCode() );
            }
        }

        return $action->processResponse( $response );
    }

    /**
     * Authorize a payment using the given credit card, customer and order data.
     *
     * @param CreditCard $card
     * @param Person $customer
     * @param Order $order
     * @return Authorization
     */
    public function authorize( CreditCard $card, Person $customer, Order $order ) : Authorization
    {
        $response = $this->execute( new AuthorizeAction( $card, $customer, $order ) );

        $authorization = new Authorization();
        $authorization->setStatus( $response->getTransactionResponse()->getStatus() );
        $authorization->setMessage( $response->getTransactionResponse()->getMessage() );
        $authorization->setError( $response->getTransactionResponse()->getError() );
        $authorization->setTransactionId( $response->getTransactionResponse()->getTransactionId() );
        $authorization->setRelatedTransactionId( $response->getTransactionResponse()->getRelatedTransactionId() );
        $authorization->setReferenceId( $response->getReferenceId() );
        $authorization->setSubmittedDate( $response->getTransactionResponse()->getSubmittedDate() );
        $authorization->setAuthorizationCode( $response->getTransactionResponse()->getAuthorizationCode() );
        $authorization->setAccountNumber( $response->getTransactionResponse()->getAccountNumber() );
        $authorization->setAccountType( $response->getTransactionResponse()->getAccountType() );
        $authorization->setAmount( $order->getAmount() );

        return $authorization;
    }

    public function capture( Authorization $authorization ) : Capture
    {
        $response = $this->execute( new CapturePriorAuthorizationAction( $authorization ) );

        $capture = new Capture();
        $capture->setStatus( $response->getTransactionResponse()->getStatus() );
        $capture->setMessage( $response->getTransactionResponse()->getMessage() );
        $capture->setError( $response->getTransactionResponse()->getError() );
        $capture->setTransactionId( $response->getTransactionResponse()->getTransactionId() );
        $capture->setRelatedTransactionId( $response->getTransactionResponse()->getRelatedTransactionId() );
        $capture->setReferenceId( $response->getReferenceId() );
        $capture->setSubmittedDate( $response->getTransactionResponse()->getSubmittedDate() );
        $capture->setAuthorizationCode( $response->getTransactionResponse()->getAuthorizationCode() );
        $capture->setAccountNumber( $response->getTransactionResponse()->getAccountNumber() );
        $capture->setAccountType( $response->getTransactionResponse()->getAccountType() );
        $capture->setAmount( $authorization->getAmount() );

        return $capture;
    }

    public function charge( CreditCard $card, Person $customer, Order $order ) : Payment
    {
        $response = $this->execute( new ChargeAction( $card, $customer, $order ) );

        $payment = new Payment();
        $payment->setStatus( $response->getTransactionResponse()->getStatus() );
        $payment->setMessage( $response->getTransactionResponse()->getMessage() );
        $payment->setError( $response->getTransactionResponse()->getError() );
        $payment->setTransactionId( $response->getTransactionResponse()->getTransactionId() );
        $payment->setRelatedTransactionId( $response->getTransactionResponse()->getRelatedTransactionId() );
        $payment->setReferenceId( $response->getReferenceId() );
        $payment->setSubmittedDate( $response->getTransactionResponse()->getSubmittedDate() );
        $payment->setAuthorizationCode( $response->getTransactionResponse()->getAuthorizationCode() );
        $payment->setAccountNumber( $response->getTransactionResponse()->getAccountNumber() );
        $payment->setAccountType( $response->getTransactionResponse()->getAccountType() );
        $payment->setAmount( $order->getAmount() );

        return $payment;
    }

    //
//    public function void() : Payment;
//
//    public function refund() : Refund;

    /**
     * @return String
     */
    public function getMerchantId(): String
    {
        return $this->merchantId;
    }

    /**
     * @return String
     */
    public function getMerchantTransactionKey(): String
    {
        return $this->merchantTransactionKey;
    }

    /**
     * @return String
     */
    public function getEnvironment(): String
    {
        return $this->environment;
    }

    private function generateReferenceId()
    {
        return 'ref' . time();
    }

    private function getMerchantAuthentication() : ANetAPI\MerchantAuthenticationType
    {
        $merchantAuthentication = new ANetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName( $this->merchantId );
        $merchantAuthentication->setTransactionKey( $this->merchantTransactionKey );
        return $merchantAuthentication;
    }

    /**
     * Get the AuthorizeNet API environment [sandbox|production].
     *
     * @return String
     */
    private function getEndpoint() : String
    {
        $endpoint = ANetConstants\ANetEnvironment::PRODUCTION;
        if( $this->environment == 'sandbox' )
        {
            $endpoint = ANetConstants\ANetEnvironment::SANDBOX;
        }
        return $endpoint;
    }

}