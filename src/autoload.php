<?php

/**
 * Bootstrap the JMS custom annotations for Object to Json mapping
 */
//
\Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace(
    'JMS\Serializer\Annotation',
    dirname(__DIR__).'/vendor/jms/serializer/src'
);

/**
 * Use this to explicitly define the location of the AuthNet transaction log file. If this is not defined then no
 * log file will be generated.
 */
//define("AUTHORIZENET_LOG_FILE", "/Users/alee/Development/Projects/Rivet/teller/transaction.log");