<?php

namespace Teller\AuthorizeNet\Action;

use net\authorize\api\contract\v1 as ANetAPI;

use Teller\AuthorizeNet\Support\GatewayResponse;

abstract class Action
{

    /**
     * @var ANetAPI\TransactionRequestType
     */
    protected $transactionRequestType;

    public function getTransactionRequestType() : ANetAPI\TransactionRequestType
    {
        return $this->transactionRequestType;
    }

    public function setTransactionRequestType( ANetAPI\TransactionRequestType $requestType )
    {
        $this->transactionRequestType = $requestType;
    }

    public function processResponse( ANetAPI\ANetApiResponseType $response ) : GatewayResponse
    {
        $gatewayResponse = GatewayResponse::make( $response );
        return $gatewayResponse;
    }
}