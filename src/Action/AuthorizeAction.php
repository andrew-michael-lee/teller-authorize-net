<?php

namespace Teller\AuthorizeNet\Action;

use net\authorize\api\contract\v1 as ANetAPI;

use Carbon\Carbon;
use Teller\AuthorizeNet\Constants;
use Teller\AuthorizeNet\Support\AuthorizeNetTypeFactory;
use Teller\Entity\CreditCard;
use Teller\Entity\Person;
use Teller\Entity\Order;

class AuthorizeAction extends Action
{
    public function __construct( CreditCard $card, Person $customer, Order $order )
    {
        $transactionRequestType = new ANetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType( 'authOnlyTransaction' );
        $transactionRequestType->setAmount( $order->getAmount() );
        $transactionRequestType->setOrder( AuthorizeNetTypeFactory::makeOrderType( $order ) );
        $transactionRequestType->setPayment( AuthorizeNetTypeFactory::makePaymentType( $card ) );
        $transactionRequestType->setBillTo( AuthorizeNetTypeFactory::makeCustomerAddressType( $customer ) );
        $transactionRequestType->addToUserFields( AuthorizeNetTypeFactory::makeUserFieldType( 'submittedAt', Carbon::now()->toDateTimeString() ) );
        $transactionRequestType->addToTransactionSettings( AuthorizeNetTypeFactory::makeSettingType( "duplicateWindow", "60" ) );
        $this->setTransactionRequestType( $transactionRequestType );
    }

}