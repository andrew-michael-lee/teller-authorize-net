<?php

namespace Teller\AuthorizeNet\Action;

use net\authorize\api\contract\v1 as ANetAPI;

use Teller\Entity\Authorization;

class CapturePriorAuthorizationAction extends Action
{
    public function __construct( Authorization $authorization )
    {
        $transactionRequestType = new ANetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType( 'priorAuthCaptureTransaction' );
        $transactionRequestType->setRefTransId( $authorization->getTransactionId() );
        $transactionRequestType->setAmount( $authorization->getAmount() );
        $this->setTransactionRequestType( $transactionRequestType );

    }
}