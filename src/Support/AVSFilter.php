<?php

namespace Teller\AuthorizeNet\Support;

/**
 * Class AVSFilter
 *
 * Provides helper methods for determining the status of AVS response codes. See http://developer.authorize.net/api/reference/
 * for more information on result codes and their meanings.
 *
 * AVS code settings are managed within the AuthorizeNet Gateway.
 * Accepted codes: A, G, P, S, U, W, X, Y, Z
 * Rejected codes: B, E, R, N
 *
 * @package Teller\AuthorizeNet\Support
 */
class AVSFilter
{
    const AVS_STREET_MATCH_NO_ZIP           = 'A'; // Address (Street) matches, ZIP does not.
    const AVS_NO_ADDRESS_PROVIDED           = 'B'; // Address information not provided for AVS check.
    const AVS_ERROR                         = 'E'; // AVS error
    const AVS_NON_US_ISSUING_BANK           = 'G'; // Non-U.S. Card Issuing Bank
    const AVS_NO_MATCH                      = 'N'; // No Match on Address (Street) or ZIP
    const AVS_NOT_APPLICABLE                = 'P'; // AVS not applicable for this transaction
    const AVS_RETRY_SYSTEM_UNAVAILABLE      = 'R'; // Retry — System unavailable or timed out
    const AVS_NOT_SUPPORTED_BY_ISSUER       = 'S'; // Service not supported by issuer
    const AVS_ADDRESS_INFO_NOT_AVAILABLE    = 'U'; // Address information is unavailable
    const AVS_9ZIP_MATCH_NO_ADDRESS         = 'W'; // Nine digit ZIP matches, Address (Street) does not
    const AVS_ADDRESS_AND_9ZIP_MATCH        = 'X'; // Address (Street) and nine digit ZIP match
    const AVS_ADDRESS_AND_5ZIP_MATCH        = 'Y'; // Address (Street) and five digit ZIP match
    const AVS_5ZIP_MATCH_NO_ADDRESS         = 'Z'; // Five digit ZIP matches, Address (Street) does not

    /**
     * Determine if the AVS response code is accepted.
     *
     * @param String $code
     * @return bool
     */
    public static function accepted( String $code ) : bool
    {
        return collect([
            AVSFilter::AVS_STREET_MATCH_NO_ZIP,
            AVSFilter::AVS_NON_US_ISSUING_BANK,
            AVSFilter::AVS_NOT_APPLICABLE,
            AVSFilter::AVS_NOT_SUPPORTED_BY_ISSUER,
            AVSFilter::AVS_ADDRESS_INFO_NOT_AVAILABLE,
            AVSFilter::AVS_9ZIP_MATCH_NO_ADDRESS,
            AVSFilter::AVS_ADDRESS_AND_9ZIP_MATCH,
            AVSFilter::AVS_ADDRESS_AND_5ZIP_MATCH,
            AVSFilter::AVS_5ZIP_MATCH_NO_ADDRESS
        ])->contains( $code );
    }

    /**
     * Determine if the AVS response code is rejected.
     *
     * @param String $code
     * @return bool
     */
    public static function rejected( String $code ) : bool
    {
        return collect([
            AVSFilter::AVS_NO_ADDRESS_PROVIDED,
            AVSFilter::AVS_ERROR,
            AVSFilter::AVS_NOT_APPLICABLE,
            AVSFilter::AVS_NO_MATCH
        ])->contains( $code );

    }

    /**
     * Determine if the AVS response code is a partial address match.
     *
     * @param String $code
     * @return bool
     */
    public static function hasPartialMatch( String $code ) : bool
    {
        return collect([
            AVSFilter::AVS_STREET_MATCH_NO_ZIP,
            AVSFilter::AVS_9ZIP_MATCH_NO_ADDRESS,
            AVSFilter::AVS_5ZIP_MATCH_NO_ADDRESS
        ])->contains( $code );

    }

    /**
     * Determine if the AVS response code is full address match.
     *
     * @param String $code
     * @return bool
     */
    public static function hasFullMatch( String $code ) : bool
    {
        return collect([
            AVSFilter::AVS_ADDRESS_AND_9ZIP_MATCH,
            AVSFilter::AVS_ADDRESS_AND_5ZIP_MATCH
        ])->contains( $code );
    }
}