<?php

namespace Teller\AuthorizeNet\Support;

use DateTime;

use net\authorize\api\contract\v1\ANetApiResponseType;
use net\authorize\api\contract\v1\TransactionResponseType;

class GatewayResponse
{

    /**
     * @var String
     */
    private $code;

    /**
     * @var array<String>
     */
    private $messages;

    /**
     * @var String
     */
    private $referenceId;

    /**
     * @var TransactionResponse;
     */
    private $transactionResponse;

    /**
     * @param ANetApiResponseType $response
     * @return GatewayResponse
     */
    public static function make( ANetApiResponseType $response ) : GatewayResponse
    {
        $gatewayResponse = new GatewayResponse();
        $gatewayResponse->setCode( $response->getMessages()->getResultCode() );
        $gatewayResponse->setMessages( $response->getMessages()->getMessage() );
        $gatewayResponse->setReferenceId( $response->getRefId() );
        $gatewayResponse->setTransactionResponse( new TransactionResponse( $response->getTransactionResponse() ) );
        return $gatewayResponse;
    }

    /**
     * @return String
     */
    public function getCode() : String
    {
        return $this->code;
    }

    /**
     * @param String $code
     */
    public function setCode( String $code )
    {
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function getMessages() : array
    {
        return $this->messages;
    }

    /**
     * @param array<String> $messages
     */
    public function setMessages( array $messages )
    {
        $this->messages = $messages;
    }

    /**
     * @return null|String
     */
    public function getReferenceId() : ?String
    {
        return $this->referenceId;
    }

    /**
     * @param null|String $referenceId
     */
    public function setReferenceId( ?String $referenceId )
    {
        $this->referenceId = $referenceId;
    }

    /**
     * @return TransactionResponse
     */
    public function getTransactionResponse() : TransactionResponse
    {
        return $this->transactionResponse;
    }

    /**
     * @param TransactionResponse $transactionResponse
     */
    public function setTransactionResponse( TransactionResponse $transactionResponse )
    {
        $this->transactionResponse = $transactionResponse;
    }



}