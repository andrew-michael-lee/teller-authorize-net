<?php

namespace Teller\AuthorizeNet\Support;

/**
 * Class CCVFilter
 *
 * Provides helper methods for determining the status of CCV response codes. See http://developer.authorize.net/api/reference/
 * for more information on result codes and their meanings.
 *
 * CCV code settings are managed within the AuthorizeNet Gateway.
 * Accepted codes: M, P, S, U
 * Rejected codes: N
 *
 * @package Teller\AuthorizeNet\Support
 */
class CCVFilter
{
    const MATCH                     = 'M'; // Match.
    const NO_MATCH                  = 'N'; // No Match.
    const NOT_PROCESSED             = 'P'; // Not Processed.
    const SHOULD_HAVE_BEEN_PRESENT  = 'S'; // Should have been present.
    const ISSUER_UNABLE_TO_PROCESS  = 'U'; // Issuer unable to process request.

    /**
     * Determine if the AVS response code is accepted.
     *
     * @param String $code
     * @return bool
     */
    public static function accepted( String $code ) : bool
    {
        return collect([
            CCVFilter::MATCH,
            CCVFilter::NOT_PROCESSED,
            CCVFilter::SHOULD_HAVE_BEEN_PRESENT,
            CCVFilter::ISSUER_UNABLE_TO_PROCESS
        ])->contains( $code );
    }

    /**
     * Determine if the AVS response code is rejected.
     *
     * @param String $code
     * @return bool
     */
    public static function rejected( String $code ) : bool
    {
        return collect([
            CCVFilter::NO_MATCH
        ])->contains( $code );

    }
}