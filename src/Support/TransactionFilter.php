<?php

namespace Teller\AuthorizeNet\Support;

/**
 * Class TransactionFilter
 *
 * Provides helper methods for determining the status of transaction response codes. See http://developer.authorize.net/api/reference/
 * for more information on result codes and their meanings.
 *
 * Possible codes: 1, 2, 3, 4
 *
 * @package Teller\AuthorizeNet\Support
 */
class TransactionFilter
{

    const APPROVED = 1;
    const DECLINED = 2;
    const ERROR = 3;
    const HELD_FOR_REVIEW = 4;

    const STATUS_LOOKUP_TABLE = [
        TransactionFilter::APPROVED => 'approved',
        TransactionFilter::DECLINED => 'declined',
        TransactionFilter::ERROR => 'error',
        TransactionFilter::HELD_FOR_REVIEW => 'heldForReview'
    ];

    /**
     * Determine if the transaction response code is approved.
     *
     * @param String $code
     * @return bool
     */
    public static function approved( String $code ) : bool
    {
        return $code == TransactionFilter::APPROVED;
    }

    /**
     * Determine if the transaction response code is rejected.
     *
     * @param String $code
     * @return bool
     */
    public static function declined( String $code ) : bool
    {
        return $code == TransactionFilter::DECLINED;
    }

    /**
     * Determine if the transaction response code is an error.
     *
     * @param String $code
     * @return bool
     */
    public static function error( String $code ) : bool
    {
        return $code == TransactionFilter::ERROR;
    }

    /**
     * Determine if the transaction response code is held for review.
     *
     * @param String $code
     * @return bool
     */
    public static function heldForReview( String $code ) : bool
    {
        return $code == TransactionFilter::HELD_FOR_REVIEW;
    }
}