<?php

namespace Teller\AuthorizeNet\Support;

use net\authorize\api\contract\v1\TransactionResponseType;

use DateTime;

class TransactionResponse
{

    /**
     * @var TransactionResponseType
     */
    private $transactionResponse;

    /**
     * @var String
     */
    private $message;

    /**
     * @var String
     */
    private $error;

    /**
     * @var array
     */
    private $userFields;

    /**
     * TransactionResponse constructor.
     *
     * @param TransactionResponseType $tr
     */
    public function __construct( TransactionResponseType $tr )
    {
        $this->transactionResponse = $tr;

        // Map the messages to something more usable.
        foreach( $tr->getMessages() as $message )
        {
            $this->message = $message->getDescription();
        }

        // Map the errors to something more usable.
        if( $tr->getErrors() != null )
        {
            $this->error = $tr->getErrors()[0]->getErrorCode() . ": " . $tr->getErrors()[0]->getErrorText();
        }

        // Map the user fields to something more usable.
        $this->userFields = [];
        foreach( $tr->getUserFields() as $field )
        {
            $this->userFields[$field->getName()] = $field->getValue();
        }
    }

    public function getStatus()
    {
        return TransactionFilter::STATUS_LOOKUP_TABLE[$this->transactionResponse->getResponseCode()];
    }

    /**
     * @return String
     */
    public function getTransactionId() : String
    {
        return $this->transactionResponse->getTransId();
    }

    public function getRelatedTransactionId() : String
    {
        return $this->transactionResponse->getRefTransId();
    }

    /**
     * @return String
     */
    public function getAccountNumber() : String
    {
        return $this->transactionResponse->getAccountNumber();
    }

    /**
     * @return String
     */
    public function getAccountType() : String
    {
        return $this->transactionResponse->getAccountType();
    }

    /**
     * @return String
     */
    public function getAuthorizationCode() : String
    {
        return $this->transactionResponse->getAuthCode();
    }

    /**
     * @return null|String
     */
    public function getMessage() : ?String
    {
        return $this->message;
    }

    /**
     * @return null|String
     */
    public function getError() : ?String
    {
        return $this->error;
    }

    /**
     * @return null|DateTime
     */
    public function getSubmittedDate() : ?DateTime
    {
        return new DateTime( $this->getUserField( 'submittedDate' ) );
    }

    /**
     * @param String $name
     * @return null|String
     */
    public function getUserField( String $name ) : ?String
    {
        return array_key_exists( $name, $this->userFields ) ? $this->userFields[$name] : null;
    }

}