<?php

namespace Teller\AuthorizeNet\Support;

use net\authorize\api\contract\v1 as ANetAPI;
use net\authorize\api\constants as ANetConstants;
use net\authorize\api\controller as ANetController;

use Exception;
use Teller\Contracts\Gateway;
use Teller\Entity\Authorization;
use Teller\Entity\CreditCard;
use Teller\Entity\Order;
use Teller\Entity\Person;

class AuthorizeNetTypeFactory
{

    public static function makeCustomerAddressType( Person $customer ) : ANetAPI\CustomerAddressType
    {
        $customerAddress = new ANetAPI\CustomerAddressType();
        $customerAddress->setFirstName( $customer->getFirstName() );
        $customerAddress->setLastName( $customer->getLastName() );
        $customerAddress->setCompany( $customer->getCompany() );
        $customerAddress->setAddress( $customer->getAddress()->getStreet() );
        $customerAddress->setCity( $customer->getAddress()->getCity() );
        $customerAddress->setState( $customer->getAddress()->getState() );
        $customerAddress->setZip( $customer->getAddress()->getPostalCode() );
        $customerAddress->setCountry( $customer->getAddress()->getCountry() );
        return $customerAddress;
    }

    public static function makeCustomerDataType( String $type, String $id, String $email ) : ANetAPI\CustomerDataType
    {
        $customerData = new ANetAPI\CustomerDataType();
        $customerData->setType( $type );
        $customerData->setId( $id );
        $customerData->setEmail( $email );
        return $customerData;
    }

    public static function makeOrderType( Order $order ) : ANetAPI\OrderType
    {
        $anOrder = new ANetAPI\OrderType();
        $anOrder->setInvoiceNumber( $order->getNumber() );
        $anOrder->setDescription( $order->getDescription() );
        return $anOrder;
    }

    public static function makePaymentType( CreditCard $card ) : ANetAPI\PaymentType
    {
        $anCreditCard = new ANetAPI\CreditCardType();
        $anCreditCard->setCardNumber( $card->getNumber() );
        $anCreditCard->setExpirationDate( $card->getExpirationDate() );
        $anCreditCard->setCardCode( $card->getCvv() );

        $payment = new ANetAPI\PaymentType();
        $payment->setCreditCard( $anCreditCard );
        return $payment;
    }

    public static function makeSettingType( String $name, String $value ) : ANetAPI\SettingType
    {
        $setting = new ANetAPI\SettingType();
        $setting->setSettingName( $name );
        $setting->setSettingValue( $value );
        return $setting;
    }

    public static function makeUserFieldType( String $name, String $value ) : ANetAPI\UserFieldType
    {
        $userFieldType = new AnetAPI\UserFieldType();
        $userFieldType->setName( $name );
        $userFieldType->setValue( $value );
        return $userFieldType;
    }
}