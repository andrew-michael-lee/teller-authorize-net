<?php

use Dotenv\Dotenv;

class TellerANTestCase extends PHPUnit\Framework\TestCase
{
    private $env;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();

        require __DIR__.'/../vendor/autoload.php';

        $this->env = new Dotenv( __DIR__ . '/../', '.env' );
        $this->env->load();

    }

    public function getEnv() : Dotenv
    {
        return $this->env;
    }
}