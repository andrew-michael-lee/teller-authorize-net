<?php

use Teller\Entity\Address;
use Teller\Entity\Authorization;
use Teller\Entity\CreditCard;
use Teller\Entity\Order;
use Teller\Entity\Person;
use Teller\AuthorizeNet\AuthorizeNetGateway;
use Teller\AuthorizeNet\Support\TransactionFilter;

class AuthorizationTest extends TellerANTestCase
{

    /**
     * @var \Teller\Contracts\Gateway
     */
    private $gateway;

    protected function setUp()
    {
        parent::setUp();
        $id = getenv( 'AUTH_NET_API_LOGIN_ID' );
        $key = getenv( 'AUTH_NET_TRANSACTION_KEY' );
        $environment = getenv( 'AUTH_NET_ENVIRONMENT' );

        $this->gateway = new AuthorizeNetGateway( $id, $key, $environment );
    }

    public function testAuthorizeWithValidData()
    {
        $card = CreditCard::make( '4111111111111111', '01', '2018', '111', 'George Jetson' );
        $person = Person::make(
            'George',
            '',
            'Jetson',
            'Acme',
            Address::make( '12345 Main St.', 'Kansas City', 'MO', '64105', 'USA') );
        $order = Order::make( '12345', 'Test Order', 25.00 );
        $authorization = $this->gateway->authorize( $card, $person, $order );

        $this->assertNotNull( $authorization );
        $this->assertEquals(  TransactionFilter::STATUS_LOOKUP_TABLE[TransactionFilter::APPROVED], $authorization->getStatus() );
    }

    public function testAuthorizeWithDuplicateTransactions()
    {
        $card = CreditCard::make( '4111111111111111', '01', '2018', '111', 'Jane Jetson' );
        $person = Person::make(
            'Jane',
            '',
            'Jetson',
            'Acme',
            Address::make( '12345 Main St.', 'Kansas City', 'MO', '64105', 'USA') );
        $order = Order::make( '12345', 'Test Order', 25.00 );
        $authorization = $this->gateway->authorize( $card, $person, $order );
        $this->assertNotNull( $authorization );
        $this->assertEquals(  TransactionFilter::STATUS_LOOKUP_TABLE[TransactionFilter::APPROVED], $authorization->getStatus() );

        $this->expectException( Exception::class );
        $this->expectExceptionCode( '11' );
        $this->expectExceptionMessage( 'A duplicate transaction has been submitted.' );
        $this->gateway->authorize( $card, $person, $order );
    }

    public function testDeclinedCard()
    {
        $card = CreditCard::make( '4111111111111111', '01', '2018', '111', 'Declined Jetson' );
        $person = Person::make(
            'Jane',
            '',
            'Jetson',
            'Acme',
            Address::make( '12345 Main St.', 'Kansas City', 'MO', '46282', 'USA') );
        $order = Order::make( '12345', 'Test Order', 25.00 );
        $authorization = $this->gateway->authorize( $card, $person, $order );
        $this->assertNotNull( $authorization );
        $this->assertEquals(  TransactionFilter::STATUS_LOOKUP_TABLE[TransactionFilter::DECLINED], $authorization->getStatus() );
    }
}