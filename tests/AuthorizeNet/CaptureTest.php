<?php

use Teller\Entity\Address;
use Teller\Entity\Authorization;
use Teller\Entity\Capture;
use Teller\Entity\CreditCard;
use Teller\Entity\Order;
use Teller\Entity\Person;
use Teller\AuthorizeNet\AuthorizeNetGateway;
use Teller\AuthorizeNet\Support\TransactionFilter;

class CaptureTest extends TellerANTestCase
{

    /**
     * @var \Teller\Contracts\Gateway
     */
    private $gateway;

    protected function setUp()
    {
        parent::setUp();
        $id = getenv( 'AUTH_NET_API_LOGIN_ID' );
        $key = getenv( 'AUTH_NET_TRANSACTION_KEY' );
        $environment = getenv( 'AUTH_NET_ENVIRONMENT' );

        $this->gateway = new AuthorizeNetGateway( $id, $key, $environment );
    }

    public function testCapturePriorAuthorizeWithValidData()
    {
        $card = CreditCard::make( '4111111111111111', '01', '2018', '111', 'John Jetson' );
        $person = Person::make(
            'John',
            '',
            'Jetson',
            'Acme',
            Address::make( '12345 Main St.', 'Kansas City', 'MO', '64105', 'USA') );
        $order = Order::make( '12345', 'Test Order', 25.00 );
        $authorization = $this->gateway->authorize( $card, $person, $order );

        $this->assertNotNull( $authorization );
        $this->assertEquals(  TransactionFilter::STATUS_LOOKUP_TABLE[TransactionFilter::APPROVED], $authorization->getStatus() );

        if( $authorization->getStatus() == TransactionFilter::STATUS_LOOKUP_TABLE[TransactionFilter::APPROVED] )
        {
            $capture = $this->gateway->capture( $authorization );
            $this->assertNotNull( $capture );
            $this->assertEquals(  TransactionFilter::STATUS_LOOKUP_TABLE[TransactionFilter::APPROVED], $capture->getStatus() );
        }
        else
        {
            $this->fail( "Authorization not approved. Unable to test capture of prior authorization." );
        }

    }

    public function testCapturePriorAuthorizeWithInvalidPriorAuthorization()
    {
        $this->expectException( Exception::class );
        $this->expectExceptionCode( '33' );
        $this->expectExceptionMessage( 'A valid referenced transaction ID is required.' );

        $authorization = new Authorization();
        $authorization->setAuthorizationCode( 'AAA111' );
        $this->gateway->capture( $authorization );
    }
}